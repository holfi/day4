--Task 1
SELECT * FROM employees;

--Task 2
SELECT * FROM employees
    WHERE first_name = 'David';

--Task 3
SELECT * FROM employees
    WHERE job_id = 'IT_PROG';

--Task 4
SELECT * FROM employees
    WHERE department_id = 50 AND salary > 4000;

--Task 5
SELECT * FROM employees
    WHERE department_id = 20 OR department_id = 30;

--Task 6
SELECT * FROM employees
    WHERE first_name LIKE '%a';

--Task 7
SELECT * FROM employees
    WHERE department_id = 50 OR department_id = 80 AND commision_pct IS NOT NULL;

--Task 8
SELECT * FROM employees
    WHERE length(replace(first_name, 'n', 'nn')) - length(first_name) > 1;

--Task 9
SELECT * FROM employees
    WHERE length(first_name) > 4;

--Task 10
SELECT * FROM employees
    WHERE salary BETWEEN 8000 AND 9000;

--Task 11
SELECT * FROM employees
    WHERE first_name LIKE '%\%%';

--Task 12
SELECT DISTINCT manager_id FROM employees
    WHERE manager_id IS NOT NULL
    ORDER BY manager_id;

--Task 13
SELECT first_name || '(' || (job_id) || ')' FROM employees;

--Task 14
SELECT * FROM employees
    WHERE length(first_name) > 10;

--Task 15
SELECT * FROM employees
    WHERE position('b' in lower(first_name)) > 0;

--Task 16
SELECT department_id,
       min(salary) min_salary,
       max(salary) max_salary,
       min(hire_date) min_hire_date,
       max(hire_date) max_hire_date,
       count(department_id) count
FROM employees
GROUP BY department_id
ORDER BY count(department_id) desc;

--Task 17
SELECT substring(first_name for 1) first_char,
       count(substring(first_name for 1)) employee_count
FROM employees
    GROUP BY first_char
    HAVING count(substring(first_name for 1)) > 1
    ORDER BY employee_count;

--Task 18
SELECT department_id, salary, count(department_id)
FROM employees
    GROUP BY department_id, salary
    HAVING count(department_id) > 1;

--Task 19
SELECT hire_date, count(department_id) employee_count
FROM employees
    GROUP BY hire_date
    ORDER BY employee_count DESC;